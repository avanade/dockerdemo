var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    _ = require('lodash'),
    us = require('underscore'),
    http = require('http')
config = require('./config.json');

var app = express();

var db = mongoose.connect(config.connectionString);
var attendance = require('./models/attendance');
var port = process.env.PORT || 10054;

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Protect, Content-Type, Accept, Authorization");
    next();
});


app.get('/api/attendance', function (req, res, next) {
    // Handle the get for this route
    var query = req.query;
    attendance.find(query, function (err, attendance) {
        if (err) {
            console.log(err);
        } else {
            res.json(attendance);
        }
    })
});

app.post('/api/attendance', function (req, res, next) {
    // Handle the post for this route
    var att = new attendance(req.body);
    att.save();

    res.status(201).send(att);
});

app.listen(port, function () {
    console.log('running on port: ' + port);
});