var mongoose = require('mongoose'), 
Schema = mongoose.Schema;

var attendanceModel = new Schema({
"firstname" : { type: String },
"lastname" : { type: String },
"attended" : { type: Boolean },
"attendeddate" : { type: Date }
});

module.exports = mongoose.model('attendance', attendanceModel);
