/** Simple example: Create a web page with form */
var gui = require ( 'easy-web-app' )

/** Initialize the framework, the default page and define a title */
var mainPage = gui.init ( 'Docker Demo' )


var viewConfig = {
  id   : 'myForm',
  title: 'Student Detail',
  type : 'pong-form',
  width: '40%'
}

var plugInConfig = {
  id: 'myFormDef',
  description: 'input name and whether or not attended',
  fieldGroups: [
      {
          columns: [
              {
                  formFields: [
                      {
                          id:    'firstname',
                          label: 'firstname',
                          type:  'text',
                          defaultVal: 'Samer'
                      },
                      {
                        id:    'lastname',
                        label: 'lastname',
                        type:  'text',
                        defaultVal: 'Zabaneh'
                    },
                    {
                      id:    'attended',
                      label: 'attended',
                      type:  'checkbox'
                  },
                  {
                    id: "attendeddate",
                    label: "attendeddate",
                    type: "date"
                  }
                  ]
              }
          ]
      }
  ],
  actions: [
      {
          id: 'myBtn',
          actionName: 'Add',
          method: 'POST',
          actionURL: 'http://localhost:10054/api/attendance'
      }
  ]
}

var columns = mainPage.addColumnsRow( 'content', '400px' )

columns.addView( viewConfig, plugInConfig  )
columns.addView( {
  id    : 'tableView',
  title : 'results',
  type  : 'pong-table',
  resourceURL:'http://localhost:10054/api',
  width: '60%',
},
{ 
  dataURL: 'attendance',
  cols: [
      { id: 'firstname', label: 'firstname', cellType: 'text' },
      { id: 'lastname', label: 'lastname', cellType: 'text' },   
      { id: 'attended', label: 'attended', cellType: 'checkbox' },             
      { id: 'attendeddate', label: 'attendeddate', cellType: 'text',  },
  ],
  rowId: 'ID',
  pollDataSec: "1"
  
})

